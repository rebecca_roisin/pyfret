.. pyFRET documentation master file, created by
   sphinx-quickstart on Fri Mar 28 11:42:16 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documentation for pyFRET: pyFRET and pyALEX
===========================================

Contents:

.. toctree::
   :maxdepth: 2

   tutorial
   FRET_reference
   ALEX_reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

