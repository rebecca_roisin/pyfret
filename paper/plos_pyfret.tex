% Template for PLoS
% Version 1.0 January 2009
%
% To compile to pdf, run:
% latex plos.template
% bibtex plos.template
% latex plos.template
% latex plos.template
% dvipdf plos.template

\documentclass[10pt]{article}

% amsmath package, useful for mathematical formulas
\usepackage{amsmath}
% amssymb package, useful for mathematical symbols
\usepackage{amssymb}

% graphicx package, useful for including eps and pdf graphics
% include graphics with the command \includegraphics
\usepackage{graphicx}

% cite package, to clean up citations in the main text. Do not remove.
\usepackage{cite}
\usepackage{hyperref}
\usepackage{color} 

\usepackage{listings}
\usepackage{color}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  language=Python,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  numbers=none,
  numberstyle=\tiny\color{gray},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

% Use doublespacing - comment out for single spacing
%\usepackage{setspace} 
%\doublespacing


% Text layout
\topmargin 0.0cm
\oddsidemargin 0.5cm
\evensidemargin 0.5cm
\textwidth 16cm 
\textheight 21cm

% Bold the 'Figure #' in the caption and separate it with a period
% Captions will be left justified
\usepackage[labelfont=bf,labelsep=period,justification=raggedright]{caption}

% Use the PLoS provided bibtex style
\bibliographystyle{plos2009}

% Remove brackets from numbering in List of References
\makeatletter
\renewcommand{\@biblabel}[1]{\quad#1.}
\makeatother


% Leave date blank
\date{}

\pagestyle{myheadings}
%% ** EDIT HERE **


%% ** EDIT HERE **
%% PLEASE INCLUDE ALL MACROS BELOW

%% END MACROS SECTION

\begin{document}

% Title must be 150 characters or less
\begin{flushleft}
{\Large
\textbf{pyFRET: An Open-Source Python Library for Analysis of Single Molecule Fluorescence Data}
}
% Insert Author names, affiliations and corresponding author email.
\\
Author1$^{1}$, 
Author2$^{2}$, 
Author3$^{3,\ast}$
\\
\bf{1} Rebecca R. Murphy Department of Chemistry, University of Cambridge, Cambridge, UK
\\
\bf{2} Magnus Kjaegaard Interdisciplinary Nanoscience Center, Aarhus, Denmark 
\\
\bf{3} Sophie E. Jackson Department of Chemistry, University of Cambridge, Cambridge, UK
\\
\bf{4} David Klenerman Department of Chemistry, University of Cambridge, Cambridge, UK
\\
$\ast$ E-mail: dk10012@cam.ac.uk
\end{flushleft}

% Please keep the abstract between 250 and 300 words
\section*{Abstract}

%Fundamental task(s) which the software accomplishes, DONE
%examples of biological insights from the use of the software, 
%details of availability, including where to download the most recent source code, the license, any operating system dependencies, and support mailing lists.

Single molecule  F\"{o}rster resonance energy transfer (smFRET) is a powerful experimental technique for studying the properties of individual biological molecules in solution. In recent years, significant progress has been made in developing new data collection methods, which significantly enrich enhance the information that can be gained from a smFRET experiment. However, adoption of smFRET techniques has so far been limited by a reliance on custom built software for data processing, which is independently developed and maintained by each research group in the field. 

Here, we present pyFRET, an open source python package for the analysis of data from single-molecule fluorescence experiments from freely diffusing biomolecules. The package provides methods for the complete analysis of a smFRET dataset, from burst selection and denoising, through data visualisation and model fitting. We provide support for both continuous excitation and alternating laser excitation (ALEX) data analysis, including burst search algorithms and stochastic denoising. We also implement the recently developed Recurrence Analysis of Single Particles (RASP) algorithm for kinetic analysis of fluorescent subppulations.

In this article, we demonstrate use of this package through analysis of a series of dual-labelled duplex DNAs, using both ALEX and continuous excitation data, to reproduce the characteristic sigmoidal FRET-eficiency curve. We also compare photon bursts from freely diffusing molecules with bursts from molecules flowed through the confocal volume, illustrating the effect of confocal dwell time on fluorescent emission.  

pyFRET is available as a package downloadable from the Python Package Index (pyPI) under a three-clause BSD licence at \url{https://pypi.python.org/pypi/pyFRET/0.1.7.1}, together with links to extensive documentation and tutorials, including example usage and test data. Additional documentation including tutorials is hosted independently on ReadTheDocs, a dedicated documentation service (\url{https://readthedocs.org/projects/pyfret/}).

% Please keep the Author Summary between 150 and 200 words
% Use first person. PLoS ONE authors please skip this step. 
% Author Summary not valid for PLoS ONE submissions.   
\section*{Author Summary}

\section*{Introduction}
% What I want to include:
% expected length: 1500 words max
% introduction to smFRET and ALEX
F\"{o}rster resonance energy transfer (FRET)~\cite{forster48} is a non-radiative energy transfer process that can occur between chromophoric molecules. The degree of energy transfer is dependent on the inter-fluorophore distance, allowing FRET to be used as a ``molecular ruler"~\cite{forster48}, to determine intramolecular distances. Since FRET was first used to measure the distance between two fluorescent dyes on individual molecules bound to a surface~\cite{ha96}, single-molecule FRET (smFRET) has become a popular tool to investigate the structure and dynamics of biomolecules, both on a surface and diffusing freely in solution~\cite{haran03, schuler02, weiss00}.

In a confocal smFRET experiment, biological molecules are labelled with two fluorescent dyes. The emission spectrum of the donor dye ($D$) is chosen to overlap with the excitation spectrum of the acceptor ($A$). When the donor and acceptor are sufficiently close in space, exciting the donor dye results in FRET and fluorescent emission from the acceptor dye. The FRET efficeicny, $E$, which descrbes the proportion of excitation energy transferred from the donor to the acceptor, depends on the distance, $r$ between the two dyes (Eq.~\ref{eq:efficiency}) and $R_0$, the F\"{o}rster distance, a dye dependent constant that describes the dye separation at which 50\% energy transfer is achieved (Fig.~\ref{fig:fig1_schematic} C)

\begin{equation}
E = \frac{1}{1 + (\frac{r}{R_0})^6} 
\label{eq:efficiency}
\end{equation}

Consequently, the distance between the two fluorophores can be determined from the ratio of donor and acceptor photons emitted during an excitation event (Eq.~\ref{eq:Eprod}).

\begin{figure}
   \begin{center}
      \includegraphics*[clip=true, width=6in]{Fig1_schematic.pdf}
      \caption{Instrumentation for a smFRET experiment. A) The confocal microscope, excitation and detection apparatus. B) Labelled molecules diffuse through the excitation volume. C) The characteristic sigmoidal dependence of FRET efficiency on dye-dye distance.}
      \label{fig:fig1_workflow}
   \end{center}
\end{figure}

Experimentally, a collimated laser beam, is used to illuminate an extremely dilute solution of labelled molecules. When a labelled molecule diffuses through the laser beam, the donor dye is excited and photons are emitted from both donor and acceptor dyes.  Emitted photons are collected through the objective and separated into donor and acceptor streams for collection and analysis (Fig.~\ref{fig:fig1_schematic} A, B). 

For a single fluorescent burst, the FRET efficiency, $E$, can be calculated as (Eq~\ref{ef:Eprod}):

\begin{equation}
E = \frac{n_A}{n_A + \gamma \cdot n_D}
\label{eq:Eprod}
\end{equation} 

for $n_A$ and $n_D$ detected acceptor and donor photons respectively and $\gamma$ an experimentally determined instrument-dependent correction factor. During the course of a smFRET experiment, several thousand fluorescent bursts are collected and used to construct FRet Efficiency histograms. These histograms can be used to identify populations of fluorescent species~\cite{ha96}, typically by fitting with multiple gaussian distributions.

Analysis of smFRET data involves several computational challenges. Firstly, photons emitted by a fluorescent molecle diffusing through the excitation volume must be identified against a noisy background. Secondly, identified bursts must be denoised, including removal of background auto-fluorescence and donor-acceptor crosstalk. Fluorescent bursts that are distorted by photobleaching or other photophysical artifacts should be identified and excluded. Multiple methods of burst selection and analysis have been developed and applied to the analysis of smFRET data~\cite{weiss00, deniz01, gell06, nir06, kapanidis05, muller05, doose07, kudryavtsev2012, eggeling01}. However, software for analysis of smFRET data has thus far been developed on an ad hoc basis, with individual groups preparing and maintaining their own analysis scripts, leading to problems typical of research programming projects~\cite{wilson06, merali10}. 

% simple interface for data analysis
% tools for burst selection, denoising, analsysis, visualisation
% short but contains all key methodologies  

% Problems with the lack of open source software and collaboration: Keep for now as want to restructure this section
% - poorly maintained code
% - code that is not reused
% - difficult to adopt new techniques as code must be implemented from scratch
% - difficult to reproduce others' results -- no access to analysis methods
% - difficult for new groups to be established as new code must be written to reimplement basic analysis techniques
% - results beween groups not comparable
% - unclear how other groups performed analysis
% - data analysis seen as secondary to experimental techniques, but methods of anlaysis can significantly alter expt outcomes, often disregarded in papers.

%Firstly: reinventing the wheel

%Firstly, there is the problem of ``reinventing the wheel"~\cite{mirams13}. Within smFRET research groups, programming ability is not a standard skill, despite the need for sophisticated data analysis and use of custom data collection hardware. It is common for researchers with programming skills to maintain their own series of data-analysis scripts which may be wholly dependent on particular hardware tools or analysis packages. Other researchers, who may lack the skills to maintain and develop even simple scripts, are dependent on black-box techniques provided by their colleagues. Consequently, data analysis is dependent on scripts written and maintained by just a few researchers. Loss of programming expertise when these team members leave can result in significant difficulties for the remaining group members, who are then dependent on poorly documented code that they do not fully understand how to use. Furthermore, the lack of available open source software often requires new researchers in the field of smFRET to completely reimplement standard analysis techniques in order to become independently productive.    

%Secondly: productivity
%Secondly, the need for many researchers to develop and maintain their own analyis tools has significant impact on research productivity. The requirement to reimplement standard analysis techniques consumes valuable time that could better be used in experimental research or in developing and benchmarking improved analysis tools. Furthermore, most researchers have no formal training in software engineering, with the result that analysis software can vary hugely in quality and is frequently poorly documented and maintained, making it difficult for other researchers to understand and use. New analysis scipts are often added in an ad hoc manner, with the result that straighforward tasks are performed using an unweildy mess of spaghetti code, transforming simple modifications into complex undertakings requring significant time investmenent. Poorly maintained code adds an additional barrier to open sharing of resources as groups are embarrassed to share low-quality software.  

% Finally: reproducibility
%Finally, there is the issue of research reproducibility. Different research groups use widely differing tools to complete relatively similar tasks. New methods of data collection and analysis are frequently developed~\cite{kapanidis05, nir06, sisamakis2010}. However, when software is not released to the community, it is difficult for researchers, who must often implement poorly described methodologies entirely from scratch, to verify results or to adopt new techniques in their own research. As a consequence, new techniques are poorly benchmarked, making it difficult to understand whether a new analysis adds quality or merely complexity, whilst adoption of useful new methods is relatively slow. These three issues of productivity, reliability and reproducibility, all linked to the problem of poorly maintained softwared and lack of software development skills, are now becoming a key bottleneck in smFRET research.  

Here we present pyFRET, an open-source python library, for the analysis of smFRET data. To our knowledge, this is the first open source software ever released by the smFRET research community. pyFRET is a small library that provides a toolkit facilitating all key steps in analysis of smFRET data: burst selection; cross-talk subtraction and burst denoising; data visualisation; and construction and simple fitting of FRET efficiency histograms. In providing this toolkit to the smFRET research community, we hope to facilitate the wider adoption of smFRET techniques in biological research as well as providing a framework for open communication about and sharing of data analsyis tools.

\section*{Design and Implementation}
\subsection*{Code Layout and Design}
pyFRET provides four key data structures (classes) for manipulation of smFRET data. The FRET data object describes two fluorescence channels, corresponding to time-bins containing photons collected from donor (the donor channel, $D$) and acceptor (the acceptor channel, $A$) fluorophores. The ALEX data object describes four fluorescence channels, corresponding to the four temporal states in a smFRET experiment using Alternating Laser Excitation (ALEX), namely the donor channel when the donor laser is switched on ($D_D$); the donor channel when the acceptor laser is switched on ($D_A$); the acceptor channel when the donor laser is on ($A_D$); and the acceptor channel when the acceptor laser is on ($A_A$). These data channels are implemented as numpy arrays, allowing efficient computations and selection operations.

Two similar classes are used for fluorescence bursts identified using the burst search algorithms. In addition to the donor and acceptor channels, the FRET bursts class type holds three additional arrays, giving the first and last bin of each burst, and the duration of each burst. These three new attributes are similarly present in the ALEX bursts object, in addition to the four fluorescent channels in the ALEX data object. In addition to the methods present for the simple FRET and ALEX objects, the burst data objects also implement methods to plot burst duration and to analyse recurrent bursts (RASP). 

The data analysis workflow is illustrated in Fig.~\ref{fig:fig1_workflow} Following initialization of data objects, background subtraction, event selection, cross-talk correction and calculation of the FRET efficiency can each be performed with a single line of code. Simple but high-quality figures can be generated in a single step.

\begin{figure}
   \begin{center}
      \includegraphics*[clip=true, width=6in]{workflow_new.pdf}
      \caption{Typical workflow for data analysis using pyFRET.}
      \label{fig:fig1_workflow}
   \end{center}
\end{figure}

\begin{figure}
   \begin{center}
      \includegraphics*[clip=true, width=6in]{6bp_example.pdf}
      \caption{Figures made using pyFRET. A) A Proximity Ratio histogram. B) A scatter-plot of FRET efficiency and fluorophore stoichiometry from ALEX data. C) A heatmap of event frequencies.  D) A 3D plot of event frequencies.}
      \label{fig:fig2_plots}
   \end{center}
\end{figure}

\subsection*{Simple Event Selection and Denoising}
\subsubsection*{FRET Data}
In the most simple smFRET experiment, fluorescently labelled molecules are excited by a laser that will excite the donor dye; all photons reaching the detectors during data acquisition binned as they are received into time-bins of length proportional to the expected dwell-time of a molecule in the confocal volume (for freely diffusing molecules, a bin-time of 1 ms is typically used). Event selection then simply involves identifying time-bins that contain sufficient photons to meet a specified criterion. Two thresholding criterion are in common use. AND thresholding selects time bins for which $n_D > T_D$ AND $n_A > T_A$ for $n_D$ and $n_A$ photons in the donor and acceptor channels respectively, and $T_D$ and $T_A$ the donor and acceptor thresholds. In a similar manner, SUM thresholding considers the sum of photons observed in the donor and acceptor channels, selecting time bins for which $n_D + n_A > T$. These thresholding techniques can be implemented using a single call to a pyFRET function: 

\begin{lstlisting}
# Simple thresholding

# define thresholds
Td = 20  # donor threshold
Ta = 20  # acceptor threshold
T = 50   # combined threshold

# AND thresholding
data.threshold_AND(Td, Ta)

# SUM thresholding
data.threshold_SUM(T)
\end{lstlisting}

Following event selection, a simple method of denoising is to subtract from each selected event the average background autofluorescence observed in each channel and the average cross-talk between the two channels:

\begin{lstlisting}
# Simple denoising

# removing autofluorescence
auto_donor = 0.5     # donor autofluorescence
auto_acceptor = 0.3  # acceptor autofluorescence
my_data.subtract_bckd(auto_donor, auto_acceptor)

# removing cross-talk
cross_DtoA = 0.05   # fractional cross-talk from donor to acceptor
cross_AtoD = 0.01   # fractional cross-talk from acceptor to donor
my_data.subtract_crosstalk(cross_DtoA, cross_AtoD)
\end{lstlisting}

This is the simplest method for event selection and denoising. However, it has several limitations. In particular, simple subtraction of constant values can lead to unphysical artifacts such as negative and fractional photon counts. Furthermore, the simple thresholding criteria for event selection are known to be biased~\cite{nir06}, so can distort downstream data analysis. 

\subsubsection*{ALEX Data}
A more sophisticated smFRET experiment uses Alternating Laser Excitation (ALEX) during data acquisition. In this method, the diffuisng fluorescent molecules are subjected to excitation from both two lasers in rapid alternation~\cite{kapanidis05}. One laser excites the donor fluorophore and the other can directly excite the acceptor fluorophore. The alternation of the laser excitation is fast on the timescale of molecular dwell-time in the confocal volume, allowing a single fluorescent molecule to receive multiple cycles of donor-acceptor direct excitation.

Instead of the two photon streams -- donor and acceptor photons -- observed in a simple smFRET experiment, in a confocal ALEX experiment, there are four streams: $F_{D_{ex}}^{D_{em}}$, $F_{D_{ex}}^{A_{em}}$, $F_{A_{ex}}^{D_{em}}$, $F_{A_{ex}}^{A_{em}}$. $F_{D_{ex}}^{D_{em}}$ and $F_{D_{ex}}^{A_{em}}$, respectively donor and acceptor emission during donor excitation, are analogous to the two original donor and acceptor photon streams in a smFRET experiment. $F_{A_{ex}}^{A_{em}}$ records acceptor emission during direct acceptor excitation, whilst $F_{A_{ex}}^{D_{em}}$ records donor emission during acceptor excitation. The presence of these extra channels provides additional information about the labelling state of molecules giving rise to fluorescent bursts, allowing exclusion of bursts from molecules lacking one of the FRET labels and bursts where one of the labels was bleached. Selection based on direct excitation of both fluorophores also removes the biases caused by simple AND or SUM thresholding. 

 pyFRET implements ALEX event selection as described in the original publication~\cite{lee06}. In brief, bursts are initially selected using a selection criterion based on the total number of photons emitted during donor and acceptor excitation:
 $F_{D_{ex}}^{D_{em}} + F_{D_{ex}}^{A_{em}} > T_D$ AND $F_{A_{ex}}^{A_{em}} > T_A$.

 Following this initial event selection, a second selection step is performed, based on the ratio of photons emitted during donor and acceptor excitation periods. The photon stoichiometry, $S$ is calculated for each burst as:

\begin{equation}
S = \frac{F_{D_{ex}}^{D_{em}} + F_{D_{ex}}^{A_{em}}}{F_{D_{ex}}^{D_{em}} + F_{D_{ex}}^{A_{em}} + F_{A_{ex}}^{A_{em}}}
\label{eq:Eprod}
\end{equation}

Events for which the stoichiometry is either very close to one or very close to zero, indicating presence of only the donor or acceptor fluorophore respectively can be excluded using a second event selection criterion: $S_{min} < S < S_{max}$

Following event selection, remaining bursts can be corrected for photon leakage and direct excitation contributions. A two-dimensional scatter plot of FRET efficiency $E$ and stoichiometery $S$ is then produced, including one-dimensional histograms of both $E$ and $S$. 

pyFRET allows each of these steps to be performed separately, however they can also be combined into a single step combining event selection, denoising, FRET efficiency calculation and plotting:

\begin{lstlisting}
# Simple ALEX analysis

g_factor = 0.95 # instrumental gamma factor
S_min = 0.2     # min accepted value of S
S_max = 0.8     # max accepted value of S
filepath = "path\to\my\file"
filename = "scatter_plot"
ALEX_data.scatter_hist(S_min, S_max, gamma=g_factor, save=True, filepath=filepath, imgname=filename, imgtype="png")
\end{lstlisting}

Example ALEX analysis scripts can be found in the supplementary information.

\subsection*{Burst Search Algorithms}
Although using time-bins that are matched to the dwell-time of molecules in the confocal volume is simple, it is not ideal, as some bursts will be split over several bins, so may be counted as separate events, or not considered for analysis. More sophisticated event selection algorithms, typically called burst search algorithms~\cite{nir06}, bin photons on a time-scale much shorter than the typical dwell time in the confocal volume and then scan the resultant photon stream for bursts of a specified duration and brightness. pyFRET implememts both All Photons Burst Search (ABBS) and a Dual Channel Burst Search (DCBS)algorithms for both ALEX data and for simple FRET data, as originally described~\cite{nir06}.

In APBS burst search for FRET data, photons from both donor and acceptor channels are considered togehter. A burst is defined according to three constants: $T$, the averaging window; $M$, the minimum number of photons within window $T$; and $L$, the minimum total number of photons required for an identified burst to be retained. These three values are used in a two-step process for burst identification.

Firstly, ``the start (respectively, the end) of a potential burst is detected when the number of photons in the averaging window of duration T is larger (respectively, smaller) than the minimum number of photons M."~\cite{nir06}. In pyFRET, this intial search is performed using the convolve method from numpy~\cite{numpy11} to provide a running sum across windows of $T$ time-bins. Following initial burst identification, a burst is retained if it contains more than $L$ photons~\cite{nir06}.

The DCBS burst search is similar, but considers the donor and acceptor channels separately. For a burst to be accepted in DCBS, both channels must simultaneoulsy meet the running sum criterion, allowing exclusion of single colour bursts and bursts where one fluorophore bleaches.

The burst search algorithms implemented for ALEX data work in a similar manner. In the ALEX APBS method, bursts are identified by considering the total number of fluorescent photons $F_{total} = F_{D_{ex}}^{D_{em}} + F_{D_{ex}}^{A_{em}} + F_{A_{ex}}^{A_{em}}$ in each time bin. Bursts are identified where the running sum (calculated using the  $F_{total}$ photon stream) in the averaging window $T$ exceeds $M$. The DCBS method considers donor excitation photons $F_{D_{ex}}^{D_{em}} + F_{D_{ex}}^{A_{em}}$ separately from photons emitted during direct acceptor excitation $F_{donor} = F_{A_{ex}}^{A_{em}}$, requiring that the running sum exceeds $M$ for both $F_{donor}$ and $F_{A_{ex}}^{A_{em}}$.

Example code for running a burst search algorithm is shown below:

\begin{lstlisting}
# Burst search using FRET data

# required parameters
T = 50             # time window (bins)
M = 50             # first threshold
L = 60             # second threshold

# calling APBS algorithm
bursts_APBS = FRET_data.APBS(T, M, L)
\end{lstlisting}

\subsection*{Stochastic Denoising of Fluorescent Bursts}
\subsection*{RASP: Recurrence Analysis of Single Particles}
A recent innovation in confocal smFRET is Reccurrence Analysis of Single Particles (RASP)~\cite{hoffmann11}. RASP uses the fact that fluorescent bursts ocurring within a small time window have a higher probility of being generated by the same molecule diffusing back through the confocal volume than from two independent fluorescent events, to access subpolulation interconversion kinetics.

RASP is a two-step process. First, initial bursts ($b_1$) with a FRET efficiency $E_{b1}$ within some defined range $\Delta(E_{b1})$ are identified. Secondly, bursts ($b_2$) occurring within a time interval (called the recurrence interval) $T = (t_1, t_2)$ of $b_1$ are identified. Analysis of the distribution of FRET efficiencies in $b_2$, the population of recurrent bursts, provides information about the interconversion rate between subpopulations.

pyFRET implements RASP using array masking, to allow efficient selection of relevant bursts. RASP can be called in a single step from a FRET bursts or ALEX bursts object:

\begin{lstlisting}
# RASP

# initial E range: 0.4 < E < 0.6
Emin = 0.4
Emax = 0.6

# Time interval for re-occurrence
# given in number of bins
Tmin = 1000
Tmax = 10000

# selcting re-ocurring bursts
recurrent_bursts = bursts_APBS.RASP(Emin, Emax, Tmin, Tmax)

# histogram of re-occurring bursts
recurrent_bursts.build_histogram(filepath, csvname, gamma=g_factor)
\end{lstlisting} 

\subsection*{Compatibilities}
pyFRET is written in Python. Both python 2 (v2.7) and python 3 (v3.3) are supported. pyFRET requires three further python libraries,  namely numpy and scipy for data manipulation, and matplotlib for data visualisation. We recommend using the free Anaconda package bundle (\url{https://store.continuum.io/cshop/anaconda/}) to install these dependencies. Detailed installation instructions can be found in the pyFRET documentation (\url{http://pyfret.readthedocs.org/en/latest/tutorial.html#installing-pyfret}). pyFRET was written and tested in a Linux environment. However, it was written to be platform independent and has also been used successfully on both Apple and Windows machines.

The lack of Open Source software in the smFRET community has led to a proliferation of esoteric file-types used for data collection and storage. To make pyFRET as usable as possible for a wide range of smFRET researchers, we provide file parsers for simple .csv and .txt file formats, as well as our custom binary format. The pyFRET data structures can be initialised using simple python arrays of time-binned photons, for users whose file format is not currently supported. The tutorial and supplementary information provide example scripts for parsing common filetypes into pyFRET objects.

\section*{Experimental Methods}
\subsection{Analysis of DNA Duplexes}
We tested the pyFRET library using DNA duplexes dual-labelled with Alexa Fluor 488 and Alexa Fluor 647. The duplex sequences and labelling sites are shown in Table~\ref{tab:dupexes}. Labelled duplexes were diluted to a concentration of 50 pM in TEN buffer (10 mM Tris, 1mM EDTA, 100 mM Nacl), pH 8.0, containing 0.0001 \% Tween-20. FRET data were collected for 15 minutes using continuous excitation at 488 nm at a power of 80 mW. Collected photons were binned online in intervals of 1 ms. ALEX data were collected for 15 minutes using alternating excitation at 488 and 640 nm, with respective laser powers of 80 and 70 mW, and a modulation rate of 0.1 ms, a dead-time of 0.1 $\mu$s and a delay compensation of 3 $\mu$s. ALEX data were then binned in intervals of 1 ms. The scripts and configuration files used to analyse these data using pyFRET can be found in the supplementary material.

\subsection{Testing the Burst Search Algorithms}
To test the burst search algorithms, we collected data under both ALEX and FRET conditions. Data were collected for 15 minutes, using the laser powers described above. For FRET data collection, laser illumination was continuous and the data were binned online into time bins of XX $\mu$s, roughly yy \% of the duration of the average burst from a freely diffusing molecule. For ALEX data, photons were similarly binned online into time bins XX $\mu$s, but the laser modulation rate was increased to ZZZ $\mu$s, with a dead-time of 0.1 $\mu$s and a delay compensation of 3 $\mu$s, corresponding to zzz modulations per short time bin. 

\subsection{Microfluidic Flow}
To test the effect of microfluidic flow on burst duration and intensity, we performed experiments under microfluidic flow. Master plates for the microfluidid devices were prepared by spin-coating X onto Y. Microfluidic devices were prepared by pouring PDMS polymer onto the pre-prepared master plates. The PDMS was allowed to set for XX hours at YY \circ in an oven, then detached from the master plate. Individual devices were prepared by slicing the PDMS into sections containing approximately six microfluidic channels, punching inlet and outlet holes for each channel, and then plasma bonding the PDMS onto plasma-cleaned microscope slides. Each microfluidic channel had a width of XX $\mu$m and a height of $\mu$m. 

Data collection under flow was performed as follows. Firstly, a microfluidic channel was filled with a dilute (50 pM) solution of labelled DNA duplex. A gel-loading tip containing a reservoir of 200 $\mu$L of duplex solution was attached at the channel inlet. The channel outlet was attached via a fluid-filled tube to a 1 mL syringe. The syringe was placed in a peristaltic pump that withdrew liquid through the microfluidic device at a rate of XX $\mu$L hr$^{-1}$, corresponding to a flow rate of XX cm s$^{-1}$. Following setup, flow rates were allowed to stabilise for 5 minutes prior to data collection.

FRET data were collected for 15 minutes using a 488 nm laser at a power of 2 mW. This increase in laser power is necessary to maximise the photons emitted during the much shorter dwell time in the confocal volume. Photons were binned online into time-bins of YYY $\mu$s. Similarly, ALEX data were collected under flow using a 488 nm at a power of 2 mW and a 640 nm laser at a power of ?? mW. The modulation rate was ZZZ $\mu$s, with a dead-time of 0.1 $\mu$s and a delay compensation of 3 $\mu$s. Photons were binned online into time bins of XXX $\mu$s.

%\subsection{Analysis of Blue Fluorescent Protein Equilibrium Unfolding} 
% Results and Discussion can be combined.

\section*{Results}

\subsection*{Benchmarking Performance with DNA Duplexes}
We tested the pyFRET library using DNA duplexes dual-labelled with Alexa Fluor 488 and Alexa Fluor 647. Event selection and denoising, calculation of FRET efficiency and the plotting and fitting of FRET efficiency histograms were performed using pyFRET. The results, shown in Fig.~\ref{fig:fig2_plots} for FRET data and Fig.~\ref{fig:fig3_plots}, demonstrate that even using the simplest event selection and denoising techniques, pyFRET is able to effectively fit histograms from single FRET populations (Fig.~\ref{fig:fig2_plots} and Fig.~\ref{fig:fig3_plots} A - E), to reproduce the characteristic sigmoidal FRET efficiency curve (Fig.~\ref{fig:fig2_plots} and Fig.~\ref{fig:fig3_plots} F).    

\begin{figure}
   \begin{center}
      \includegraphics*[clip=true, width=6in]{FRET_AND.pdf}
      \caption{Analysis of FRET data from DNA duplexes using pyFRET. A - E: Fitted FRET histograms from DNA duplexes labelled with a dye-dye separation of 4, 6, 8, 10 and 12 base pairs respectively. F) Characteristic sigmoidal curve of FRET efficiency against dye-dye distance.}
      \label{fig:fig2_plots}
   \end{center}
\end{figure}

\begin{figure}
   \begin{center}
      \includegraphics*[clip=true, width=6in]{FRET_ALEX.pdf}
      \caption{Analysis of ALEX data from DNA duplexes using pyALEX. A - E: Fitted FRET histograms from DNA duplexes labelled with a dye-dye separation of 4, 6, 8, 10 and 12 base pairs respectively. F) Characteristic sigmoidal curve of FRET efficiency against dye-dye distance.}
      \label{fig:fig3_plots}
   \end{center}
\end{figure}

\subsection*{Analysis of Equilibrium Unfolding of Blue Fluorescent Protein}



% You may title this section "Methods" or "Models". 
% "Models" is not a valid title for PLoS ONE authors. However, PLoS ONE
% authors may use "Analysis" 
\section*{Availability and Future Directions}
pyFRET is available to download from PyPI under an open source MIT/BSD licence from the Python Package Index (\url{https://pypi.python.org/pypi/pyfret0.1.0}). Documentation can also be found here, whilst a more extensive tutorial, including example scripts, can be found on our website (\url{http://rrm33.user.srcf.net/}) or in the Supplementary Information.

pyFRET currently provides basic tools for burst selection and denoising, based on simple thresholding and noise subtraction techniques. We are aware that more sophisticated methodologies exist and are currently working to produce and open source burst selection algorithm based on photon arrival times~\cite{nir06} as well as stochastic denoising algorithms~\cite{kudryavtsev2012}. We have also developed a novel analysis method based on Bayesian statistics~\cite{murphy14}, for which source code is available (\url{https://bitbucket.org/rebecca_roisin/fret-inference}) and which we intend to fold into the pyFRET library. We are also working to increase support for the wide variety of file formats that result from custom-built data collection hardware. 

smFRET is a fast-developing and active research field and we are keen to support scientific progress through development of high-quality usable software. We are keen to work with others to enable their use of and contribution to the pyFRET library. We welcome requests for custom analysis requirements and are happy to support others who wish to contribute additional code to the pyFRET infrastucture. 

% Do NOT remove this, even if you are not including acknowledgments
\section*{Acknowledgments}
RRM would like to thank BBSRC for PhD funding.


%\section*{References}
% The bibtex filename
\bibliography{plos_pyfret}

\section*{Figure Legends}
%\begin{figure}[!ht]
%\begin{center}
%%\includegraphics[width=4in]{figure_name.2.eps}
%\end{center}
%\caption{
%{\bf Bold the first sentence.}  Rest of figure 2  caption.  Caption 
%should be left justified, as specified by the options to the caption 
%package.
%}
%\label{Figure_label}
%\end{figure}


\section*{Tables}
%\begin{table}[!ht]
%\caption{
%\bf{Table title}}
%\begin{tabular}{|c|c|c|}
%table information
%\end{tabular}
%\begin{flushleft}Table caption
%\end{flushleft}
%\label{tab:label}
% \end{table}

\end{document}

